/* tslint:disable */

declare var Object: any;
export interface VehiclesInterface {
  "idvehicle"?: number;
  "brand": string;
  "model": string;
  "passengerscapacity"?: string;
  "categoria": number;
  "price"?: number;
  "year"?: number;
  "wight"?: number;
}

export class Vehicles implements VehiclesInterface {
  "idvehicle": number;
  "brand": string;
  "model": string;
  "passengerscapacity": string;
  "categoria": number;
  "price": number;
  "year": number;
  "wight": number;
  constructor(data?: VehiclesInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Vehicles`.
   */
  public static getModelName() {
    return "Vehicles";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Vehicles for dynamic purposes.
  **/
  public static factory(data: VehiclesInterface): Vehicles{
    return new Vehicles(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Vehicles',
      plural: 'Vehicles',
      path: 'Vehicles',
      idName: 'idvehicle',
      properties: {
        "idvehicle": {
          name: 'idvehicle',
          type: 'number'
        },
        "brand": {
          name: 'brand',
          type: 'string'
        },
        "model": {
          name: 'model',
          type: 'string'
        },
        "passengerscapacity": {
          name: 'passengerscapacity',
          type: 'string'
        },
        "categoria": {
          name: 'categoria',
          type: 'number'
        },
        "price": {
          name: 'price',
          type: 'number'
        },
        "year": {
          name: 'year',
          type: 'number'
        },
        "wight": {
          name: 'wight',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
