import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Vehicles, VehiclesApi, LoopBackFilter } from './lbsdk/index';
@Injectable()
export class VehicleService {

  constructor(private vehicleApi: VehiclesApi) { }

  find(filter?: LoopBackFilter): Observable<Vehicles[]> {
    return this.vehicleApi.find(filter);
  }
  create(vehicle: Vehicles): Observable<Vehicles> {
    return this.vehicleApi.create(vehicle);
  }
  update(vehicle: Vehicles): Observable<Vehicles> {
    return this.vehicleApi.patchAttributes(vehicle.idvehicle, vehicle);
  }
  delete(vehicle: Vehicles): Observable<Vehicles> {
    return this.vehicleApi.deleteById(vehicle.idvehicle)
  }
}
