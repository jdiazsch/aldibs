import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//Primeng
import {
  DataTableModule,
  DialogModule,
  DropdownModule
} from 'primeng/primeng'

//Components
import { AppComponent } from './app.component';
import { VehicleViewComponent } from './vehicle-view/vehicle-view.component';
import { AltaModificaVehicleComponent } from './vehicle-view/alta-modifica-vehicle.component';

//Services
import { SDKBrowserModule } from './services/lbsdk/';
import { VehicleService } from './services/vehicle.service';

@NgModule({
  declarations: [
    AppComponent,
    VehicleViewComponent,
    AltaModificaVehicleComponent
  ],
  imports: [
    SDKBrowserModule.forRoot(),
    NgbModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    DialogModule,
    DropdownModule
  ],
  providers: [
    VehicleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
