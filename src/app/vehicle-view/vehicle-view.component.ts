import { Component, OnInit } from '@angular/core';
import { VehicleService } from '../services/vehicle.service';
import { Vehicles } from '../services/lbsdk/index';

@Component({
  selector: 'app-vehicle-view',
  templateUrl: './vehicle-view.component.html',
  styleUrls: ['./vehicle-view.component.css']
})
export class VehicleViewComponent implements OnInit {

  showPopUp: boolean = false;
  vehicles: Vehicles[] | null = null;
  selectedVehicle: Vehicles = new Vehicles();

  constructor(private vehicleService: VehicleService) { }

  ngOnInit() {
    this.listVehicles();
  }

  listVehicles() {
    this.vehicleService.find()
      .subscribe((vehicles: Vehicles[]) => {
        this.vehicles = vehicles;
      })
  }

  addNewVehicle() {
    this.selectedVehicle = new Vehicles();
    this.showPopUp = true;
  }

  updateVehicle() {
    this.selectedVehicle.idvehicle ? this.showPopUp = true : alert("No se selecciono ningún vehículo");
  }

  deleteVehicle() {
    if (this.selectedVehicle.idvehicle) {
      this.vehicleService.delete(this.selectedVehicle)
        .subscribe(() => {
          this.listVehicles();
        })
    } else {
      alert("No se selecciono ningún vehículo");
    }
  }

  closePopUp(event) {
    this.selectedVehicle = new Vehicles();
    this.showPopUp = false;
    this.listVehicles();
  }

  onRowSelect(event) {
    this.selectedVehicle = event.data;
    console.log("VEHICULO SELECCIONADO:", this.selectedVehicle)
  }
}
