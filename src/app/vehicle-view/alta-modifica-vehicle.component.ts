import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { VehicleService } from '../services/vehicle.service';
import { Vehicles } from '../services/lbsdk/index';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';

@Component({
    selector: 'alta-modifica-vehicle',
    templateUrl: './alta-modifica-vehicle.component.html',
    styleUrls: ['./vehicle-view.component.css']
})
export class AltaModificaVehicleComponent implements OnInit {

    @Input() popUp: boolean;
    @Input() vehicle: Vehicles;
    @Output() onClose = new EventEmitter<any>();
    vehicleForm: FormGroup;
    categorias: SelectItem[] = [];

    constructor(
        private vehicleService: VehicleService,
        private fb: FormBuilder
    ) {
        this.vehicleForm = fb.group({
            'brand': ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
            'model': ['', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
            'passengerscapacity': ['', Validators.pattern(/^[0-5]+$/)],
            'categoria': ['', [Validators.required]],
            'price': ['',[Validators.required, Validators.pattern(/^[0-5]+$/)]],
            'year': ['',[Validators.required, Validators.pattern(/^[0-5]+$/)]],
            'wight': ['',[Validators.required, Validators.pattern(/^[0-5]+$/)]]            
        })
    }

    ngOnInit() {
        this.listCategories();
    }

    listCategories() {
        this.categorias.push({ value: 0, label: "Motocicleta" });
        this.categorias.push({ value: 1, label: "Automóvil" });
        this.categorias.push({ value: 2, label: "SRV" });
        this.categorias.push({ value: 3, label: "Camioneta" });
        this.categorias.push({ value: 4, label: "Camión" });
        this.categorias.push({ value: 4, label: "Micro" });
    }

    save() {
        if (!this.vehicle.idvehicle) {
            this.vehicleService.create(this.vehicle)
                .subscribe(() => this.onClosePopUp())
        } else {
            this.vehicleService.update(this.vehicle)
                .subscribe(() => this.onClosePopUp())
        }
    }

    onClosePopUp() {
        this.onClose.emit();
    }
}